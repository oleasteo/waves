package model.geometry;

public class Movable extends Point {

  private double vx, vy;

  public Movable(double x, double y, double vx, double vy) {
    super(x, y);
    this.vx = vx;
    this.vy = vy;
  }

  public void update(double steps) { this.move(vx * steps, vy * steps); }

  public double getVX() { return vx; }

  public void setVX(double vx) { this.vx = vx; }

  public double getVY() { return vy; }

  public void setVY(double vy) { this.vy = vy; }

}
