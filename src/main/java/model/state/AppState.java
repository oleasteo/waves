package model.state;

public class AppState {

  private boolean running = true;

  public boolean isRunning() { return running; }

  public void stopRunning() { this.running = false; }

}
