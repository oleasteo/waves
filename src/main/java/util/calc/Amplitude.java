package util.calc;

import model.Wave;
import model.element.Emitter;
import model.geometry.Point;

import java.util.List;

public class Amplitude {

  public static double getInterference(List<Emitter> emitters, double x, double y) {
    double value = 0;
    for (Emitter emitter : emitters) { value += getAmplitude(emitter, x, y); }
    return value;
  }

  public static double getAmplitude(Emitter emitter, double x, double y) {
    if (!emitter.isVisible()) { return 0; }
    double value = getAmplitude(emitter.getAttachedWave(), emitter, x, y);
    for (Wave wave : emitter.getDetachedWaves()) { value += getAmplitude(wave, wave, x, y); }
    return value;
  }

  private static double getAmplitude(Wave wave, Point location, double tX, double tY) {
    if (wave.isInitial()) { return 0; }
    // see assets/math-geometry.is.svg draft for insight on mathematical details of this method
    double lX = location.getX(), lY = location.getY();
    // normalize L := (0,0)
    double oX = wave.getOriginX() - lX, oY = wave.getOriginY() - lY;
    tX -= lX;
    tY -= lY;
    if (tX == 0) { // m is infinity
      if (tY == 0) { return getAmplitude(wave, 1); }
      // todo fix m = infinity case
      return 0;
    }
    // calculate basic values
    double m = tY / tX;
    double divisor = 1 + m * m;
    // todo cache oX * oX + oY * oY (originSqDistance)
    double u = (oX + m * oY) / divisor, q = (oX * oX + oY * oY - wave.getOriginSqRadius()) / divisor;
    double radiant = u * u - q;
    if (radiant < 0) { return 0; }
    double cX1 = u + Math.sqrt(radiant);
    double cX2 = u - Math.sqrt(radiant);
    // calculate amplitude
    double amplitude = 0;
    boolean signTX = tX >= 0;
    // use cX1 only if tX and cX1 are both at the same direction from lX
    if (signTX ^ cX1 < 0) {
      double rel1 = (cX1 - tX) / cX1;
      // use cX1 only if tX is between cX1 and lX
      if (rel1 > 0) { amplitude += getAmplitude(wave, rel1); }
    }
    // same for cX2
    if (signTX ^ cX2 < 0) {
      double rel2 = (cX2 - tX) / cX2;
      if (rel2 > 0) { amplitude += getAmplitude(wave, rel2); }
    }
    return amplitude;
  }

  private static double getAmplitude(Wave wave, double relative) {
    return Math.sin(relative * wave.getBirthAgo() * wave.getFrequencyTimes2Pi());
  }

}
