package view;

import model.state.Base;

import javax.swing.*;
import java.awt.*;

public class ActionsScrollPane extends ScrollPane {

  private class Pane extends Panel {

    private JToggleButton pauseToggleBtn = new JToggleButton("Pause");

    private Pane() {
      setLayout(null);
      populate();
    }

    private void updateState() {
      pauseToggleBtn.setSelected(STATE.isPaused());
    }

    public void doLayout() {
      int width = ActionsPanel.MINIMUM_SIZE.width - 2;
      int y = 5;
      pauseToggleBtn.setBounds(0, y, width, 50);

      Dimension viewportSize = ((ScrollPane) this.getParent()).getViewportSize();
      setMinimumSize(viewportSize);
    }

    private void populate() {
      add(pauseToggleBtn);
      pauseToggleBtn.addActionListener(e -> STATE.togglePause());
    }

  }

  private final Pane pane;
  private final Base STATE;

  public ActionsScrollPane(Base state) {
    STATE = state;
    add(pane = new Pane());
  }

  public void updateState() {
    pane.updateState();
  }

}
