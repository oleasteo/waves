package view.listener;

import model.state.Base;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class FrameWindowListener implements WindowListener {

  private final Base STATE;

  public FrameWindowListener(Base state) { STATE = state; }

  public void windowOpened(WindowEvent windowEvent) {}

  public void windowClosing(WindowEvent windowEvent) { STATE.getAppState().stopRunning(); }

  public void windowClosed(WindowEvent windowEvent) {}

  public void windowIconified(WindowEvent windowEvent) {}

  public void windowDeiconified(WindowEvent windowEvent) {}

  public void windowActivated(WindowEvent windowEvent) {}

  public void windowDeactivated(WindowEvent windowEvent) {}

}
