package view;

import model.state.Base;
import model.element.Element;
import view.listener.CanvasKeyListener;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

public class Canvas extends java.awt.Canvas {

  private final Point ZERO = new Point(0, 0);
  private final Base STATE;
  private final AmplitudeImage AMPLITUDE_IMAGE;

  private BufferedImage image;

  public Canvas(Base state) {
    STATE = state;
    AMPLITUDE_IMAGE = new AmplitudeImage(STATE, ZERO);
    addKeyListener(new CanvasKeyListener(STATE));
  }

  public void doLayout() {
    update();
    repaint();
  }

  public synchronized void update() { image = AMPLITUDE_IMAGE.getImage(getWidth(), getHeight()); }

  public void paint(Graphics g) {
    super.paint(g);
    g.drawImage(image, 0, 0, null);
    drawMovables(g);
  }

  private void drawMovables(Graphics g) {
    List<Element> elements = STATE.getElements();
    for (Element element : elements) { drawMovable(g, element); }
  }

  private void drawMovable(Graphics g, Element element) {
    int scale = STATE.getUIState().getScale();
    int width = getWidth(), height = getHeight();
    int x = (int) ((element.getX() - ZERO.x) * scale), y = (int) ((element.getY() - ZERO.y) * scale);
    double vx = element.getVX() * scale, vy = element.getVY() * scale;

    if (x > 0 && x < width && y > 0 && y < height) {
      g.setColor(element.getColor());
      g.fillRect(x - 2, y - 2, 5, 5);
      if (vx != 0 || vy != 0) { g.drawLine(x, y, (int) (x + vx * 1000), (int) (y + vy * 1000)); }
      g.setColor(Color.BLACK);
      g.drawRect(x - 2, y - 2, 5, 5);
    }
  }

  public void exit() { AMPLITUDE_IMAGE.exit(); }

}
